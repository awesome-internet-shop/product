package com.shop.product.controller;

import com.shop.product.enumeration.ProductStatus;
import com.shop.product.model.dto.Product;
import com.shop.product.model.dto.ProductCreationRequest;
import com.shop.product.service.ProductAdministrationService;
import com.shop.product.service.ProductService;
import jakarta.validation.Valid;
import jakarta.validation.constraints.Min;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Validated
@RestController
@RequiredArgsConstructor
@RequestMapping("admin/products")
public class ProductAdministrationController {

    private final ProductAdministrationService productAdministrationService;
    private final ProductService productService;

    @PostMapping
    public void createProduct(@Valid @RequestBody ProductCreationRequest productCreationRequest) {
        productAdministrationService.createProduct(productCreationRequest);
    }

    @GetMapping
    public Page<Product> getAllProducts(@RequestParam @Min(1) int pageNumber,
                                        @RequestParam @Min(1) int pageSize) {
        return productService.getAllProducts(pageNumber, pageSize);
    }

    @PatchMapping("{productId}")
    public void updateProductStatus(@PathVariable Long productId,
                                    @RequestParam ProductStatus status) {
        productAdministrationService.updateProductStatus(productId, status);
    }

    @DeleteMapping("{productId}")
    public void deleteProduct(@PathVariable Long productId) {
        productAdministrationService.deleteProduct(productId);
    }
}
