package com.shop.product.mapper;

import com.shop.product.model.dto.Product;
import com.shop.product.model.enity.ProductEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.util.Currency;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    @Mapping(source = "currencyCode", target = "currency", qualifiedByName = "currencyQualifier")
    Product entityToDto(ProductEntity entity);

    @Mapping(source = "currency.currencyCode", target = "currencyCode")
    ProductEntity dtoToEntity(Product dto);

    @Named("currencyQualifier")
    default Currency getCurrencyFromCurrencyCode(String currencyCode) {
        return Currency.getInstance(currencyCode);
    }
}
