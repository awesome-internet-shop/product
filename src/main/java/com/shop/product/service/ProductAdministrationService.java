package com.shop.product.service;

import com.shop.product.enumeration.ProductStatus;
import com.shop.product.mapper.ProductMapper;
import com.shop.product.model.dto.Product;
import com.shop.product.model.dto.ProductCreationRequest;
import com.shop.product.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Currency;

@Service
@RequiredArgsConstructor
public class ProductAdministrationService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    public void createProduct(ProductCreationRequest productCreationRequest) {
        // fixup делать это в ProductRequestMapper
        final Product product = new Product()
                .setName(productCreationRequest.getName())
                .setDescription(productCreationRequest.getDescription())
                .setPrice(productCreationRequest.getPrice())
                .setCurrency(Currency.getInstance(productCreationRequest.getCurrencyCode()))
                .setStatus(productCreationRequest.getStatus());
        productRepository.saveAndFlush(productMapper.dtoToEntity(product));
    }

    public void updateProductStatus(Long productId, ProductStatus newStatus) {
        productRepository.updateProductStatusById(productId, newStatus);
    }

    public void deleteProduct(Long productId) {
        productRepository.deleteById(productId);
    }
}
