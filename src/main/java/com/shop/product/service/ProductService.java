package com.shop.product.service;

import com.shop.product.exception.ProductNotFoundException;
import com.shop.product.mapper.ProductMapper;
import com.shop.product.model.dto.Product;
import com.shop.product.repository.ProductRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    public Page<Product> getAllProducts(int pageNumber, int pageSize) {
        return productRepository.findAll(PageRequest.of(pageNumber, pageSize))
                .map(productMapper::entityToDto);
    }

    public Product getProduct(Long productId) {
        return productRepository.findById(productId)
                .map(productMapper::entityToDto)
                .orElseThrow(ProductNotFoundException::new);
    }
}
