package com.shop.product.repository;

import com.shop.product.enumeration.ProductStatus;
import com.shop.product.model.enity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductRepository extends JpaRepository<ProductEntity, Long> {

    @Modifying
    @Query("UPDATE ProductEntity p SET p.status = :productStatus WHERE p.id = :id")
    void updateProductStatusById(@Param("id") Long id,
                                 @Param("productStatus") ProductStatus productStatus);
}
