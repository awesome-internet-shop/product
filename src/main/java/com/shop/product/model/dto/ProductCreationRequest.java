package com.shop.product.model.dto;

import com.shop.product.enumeration.ProductStatus;
import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;

@Data
@Accessors(chain = true)
public class ProductCreationRequest {

    @NotBlank
    private String name;
    @NotBlank
    private String description;
    @Min(0)
    @NotNull
    private BigDecimal price;
    @NotBlank
    private String currencyCode;
    @NotNull
    private ProductStatus status;
}
