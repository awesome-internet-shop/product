package com.shop.product.model.dto;

import com.shop.product.enumeration.ProductStatus;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Currency;

@Data
@Accessors(chain = true)
public class Product {

    private Long id;
    private String name;
    private String description;
    private BigDecimal price;
    private Currency currency;
    private ProductStatus status;
}
