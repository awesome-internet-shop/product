package com.shop.product.enumeration;

public enum ProductStatus {

    ACTIVE, DELETED
}
